<xsl:stylesheet version="1.0" xmlns:tns="http://aerolineas-latinoamericanas/contract/messages"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes="#default tns">
    
    <xsl:output method ="text"/>
    <xsl:template match="/">
    
        <xsl:number format="0000000001"/>
        <xsl:value-of select="tns:aircratline-message/tns:flight-leg/tns:domain-events-info/tns:on-air-shopping/tns:credit-card-number"/>
        <xsl:value-of select="tns:aircratline-message/tns:flight-leg/tns:domain-events-info/tns:on-air-shopping/tns:transaction-date"/>
        <xsl:value-of select="tns:aircratline-message/tns:flight-leg/tns:domain-events-info/tns:on-air-shopping/tns:value"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="tns:aircratline-message/tns:flight-leg/tns:domain-events-info/tns:on-air-shopping/tns:description"/>
        
    </xsl:template>
</xsl:stylesheet>